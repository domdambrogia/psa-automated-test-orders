## Automated Test Orders

This utilizes puppeteer to spin up an instance of chromium and mindlessly create test orders just as a customer would. It take about 30 seconds to mock making an order.

### Get Started:
_To create FFL orders you'll need an account with a shipping addressed saved on file. This can be created manually by signing up through the account page [here](https://staging.palmettostatearmory.com/customer/account/login/referer/)_

The existing settings will create orders right out of the box with the following two commands:

    npm install;
    # For orders with no FFL items:
    npm run create-orders;
    # For orders with FFL items OR without FFL items:
    npm run create-roders -- --user=<login email> --password=<login password>

If you need multiple orders, you can loop through it like so:

    for i in {1..5}; do npm run create-orders; done;

If you need multiple orders *in parallel*, you can dispatch a group like so:

    for i in {1..3}; do npm run create-orders &; done;

### Configuration:
Most of the fields in `config.json` are self explanatory i.e.. card, billing and homepage.

Products have a specific structure, for configurable products you'll need to explain the attribute id and value to pick when on the product page. For example, on [this page](https://staging.palmettostatearmory.com/psa-no-step-on-snek-short-sleeve-t-shirt-psasnektee.html) if you inspect the HTML you'll find this:

    <select name="super_attribute[141]" id="attribute141" class="required-entry super-attribute-select">
        <option value="">Choose an Option...</option>
        <option value="31">Small</option>
        <option value="30">Medium</option>
        <option value="29">Large</option>
        <option value="28">X-Large</option>
        <option value="27">2X-Large</option>
        <option value="26">3X-Large</option>
    </select>

In the `config.json` file you'll see this:

    ...
    "attributes": [ { "id": "141", "value": "31" } ]`
    ...

That means when that product page is visited, value 31 (Small) will be chosen for attribute 141. For a simple product, you can omit the attributes property in the JSON, since it won't be used.

The `headless` and `viewport` properties are specific to puppeteer, you can read about them [here](https://pptr.dev/). I would recommend keeping the viewport the same but if you don't want to have the window pop up when an order is being created you can set `headless` to `true` and it will suppress the window. If you run into issues after changing configuration values, an easy way to see where you're messing up is be enabling `headless` and watching the execution.

### What's Missing:
 - Gift Cards, lesser of a priority. I'd like to omit it, but if it's necessary I can see what I can do.
 - Tokenex checkout (probably doesn't matter to non magento devs), the checkout method must be USAePay which differs slightly from magento production but shouldn't affect netsuite testing.

