const puppeteer = require('puppeteer');
const settings = require('./src/util/settings');
const actions = require('./src/action/all');

(async () => {
    const browser = await puppeteer.launch({
        headless: settings.headless,
        devtools: settings.devtools
    });

    var page = await browser.newPage();
    page.setViewport(settings.viewport);
    page.setCacheEnabled(settings.cache);
    // Keep these here. Once the modal closes it won't show up again.
    await actions.browse.goHome(page);
    await actions.browse.closeEmailModal(page);

    await actions.browse.addNonFflToCart(page);
    await actions.checkout.checkoutAsGuest(page);

    await browser.close();
})();
