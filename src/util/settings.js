const path = require('path');
const argv = require('minimist')(process.argv.slice(2));

/**
 * Get the settings object.
 * @return Object
 */
const getSettings = () => {
    const cli = getCliParams();
    const file = require(path.join(__dirname, '..', '..', 'config.json'));
    const combined = Object.assign(file, cli);

    return combined;
};

/**
 * Get the settings passed through the CLI.
 * @return Object
 */
const getCliParams = () => ({
    credentials: {
        user: argv.user || '',
        password: argv.password || ''
    }
});

module.exports = getSettings();
