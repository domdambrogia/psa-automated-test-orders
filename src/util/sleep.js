/**
 * Sleep for a specified amount of milliseconds.
 * @param integer time
 * @return Promise
 */
async function sleep (time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

module.exports = sleep;
