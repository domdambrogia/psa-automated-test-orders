const settings = require('../util/settings');
const sleep = require('../util/sleep');

/**
 * Go home by clicking on the logo in the header.
 *
 * @return Page
 */
async function goHome (page) {
    await page.goto(settings.homepage, { waitUntil: 'networkidle2' });
}

/**
 * Grab a random product declared in the cyrpess config.
 *
 * @param integer ffl
 * @return object
 */
function _getDeclaredProduct (ffl) {
    const products = settings.products;
    const rand = (min, max) => Math.floor(Math.random() * (max - min) + min);
    const i = rand(0, products.length);
    const p = products[i];

    // if FFL needs weren't met, try agin. shouldn't take > 3x.
    return (ffl !== null && p.ffl !== ffl) ? _getDeclaredProduct(ffl) : p;
}

/**
 * Visit a random product declared in the cypress config.
 *
 * @param Page page
 * @param bool ffl
 * @return object
 */
async function visitDeclaredProduct (page, ffl = null) {
    const product = _getDeclaredProduct(ffl);
    await page.goto(`${settings.homepage}/${product.url}`, { waitUntil: 'networkidle2' });
    return product;
}

/**
 * Close the email modal on the page if it exists.
 * @param Page page
 * @return void
 */
async function closeEmailModal (page, depth = 0) {
    // This is recursive, don't recurse more than 5 times (5 seconds)
    if (depth < 5) {
        // Wait 1 second for the modal, if it exists close it.
        const elements = await page.$$('.ltkmodal-close');
        await sleep(1000);
        if (elements.length) {
            const recurse = () => closeEmailModal(page, depth + 1);
            await page.click('.ltkmodal-close').catch(recurse);
        }
    }
}

/**
 * Select configurable options for a product.
 *
 * @param Page page
 * @param object options
 * @return void
 */
async function _selectOptions (page, options) {
    const selects = Object.keys(options);
    // key value format should be selector => value:
    // #attribute141 => 41; <select id="attribute141"> => <option value="41">
    for (var i = 0; i < selects.length; i++) {
        var select = selects[i];
        var value = options[select];
        // the first option shows by default, the target loads async.
        var fn = s => document.querySelectorAll(`${s} > option`).length > 1;
        await page.waitForFunction(fn, {}, select);
        await page.select(select, value);
    }
}

/**
 * Add a non ffl item to the cart.
 *
 * @param Page page
 * @return void
 */
async function addNonFflToCart (page) {
    const product = await visitDeclaredProduct(page, false);

    if (product.options !== null) {
        await _selectOptions(page, product.options);
    }

    await page.waitFor(1000);
    await page.waitFor(() => {
        if (!document.querySelector('.qty-control #qty')) {
            return false;
        }

        document.querySelector('.qty-control #qty').value = 1;
        return true;
    });

    await page.click('#product-addtocart-button');
    await page.waitForNavigation({ waitUntil: 'networkidle2' });
}

module.exports = {
    addNonFflToCart,
    closeEmailModal,
    goHome,
    visitDeclaredProduct
};
