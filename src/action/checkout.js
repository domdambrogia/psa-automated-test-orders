const settings = require('../util/settings');

/**
 * Go to the cart page, based on the home page declared in the settings.
 *
 * @param Page page
 * @return void
 */
async function goToCart (page) {
    const url = await page.url();

    if (!url.includes('/checkout/cart')) {
        const cartLink = `${settings.homepage}/checkout/cart`;
        await page.goto(cartLink, { waitUntil: 'networkidle2' });
    }
}

/**
 * Checkout as a guest. This method assumes there are no FFL items in the cart.
 *
 * @param Page page
 */
async function checkoutAsGuest (page) {
    await goToCart(page);
    await page.waitFor(() => !!document.querySelectorAll('.cart-summary .nra-donation.fieldset').length);
    await page.click('button[title="Checkout as guest"]');
    await page.waitFor(() => !!document.querySelectorAll('#customer-email').length);
    await inputGuestShippingInfo(page);
    await inputGuestBillingInfo(page);
    await reviewAndSubmit(page);
}

/**
 * Navigate through the final stage of the checkout and submit order.
 *
 * @param Page page
 * @return void
 */
async function reviewAndSubmit (page) {
    await page.$eval('#agreement_3', c => { c.checked = true; });
    await page.click('button.action.place-order');
    await page.waitForNavigation({ waitUntil: 'networkidle2' });
}

/**
 * Input the guest billing information.
 *
 * @param Page page
 * @return void
 */
async function inputGuestBillingInfo (page) {
    const cc = settings.card;
    try {
        await page.waitFor(() => document.querySelector('#zs_usaepay') !== null, 15000);
    } catch (e) {
        console.error([
            'It seems the non tokenex version of USAePay is unavailable.',
            'Tokenex uses an iframe that can not be interacted with via javascript.',
            'Please make sure USAePay is enabled and tokenex is disabled.'
        ].join('\n'));

        process.exit(1);
    }

    await page.evaluate(() => document.querySelector('#zs_usaepay').click());
    await page.waitFor(() => document.querySelector('#zs_usaepay_cc_number') !== null);
    await page.waitFor(1500);
    await page.type('#zs_usaepay_cc_number', cc.number);
    await page.select('#zs_usaepay_expiration', cc.month);
    await page.select('#zs_usaepay_expiration_yr', cc.year);
    await page.type('#zs_usaepay_cc_cid', cc.cvc);
    await page.click('#payment-continue-buttons-container button.action.continue');
    await page.waitFor(() => !document.querySelectorAll('#checkout-loader').length);
}

/**
 * Input shipping info for the guest checkout.
 *
 * @param Page page
 * @return void
 */
async function inputGuestShippingInfo (page) {
    const user = settings.existingUser;
    const opts = { delay: 100 };

    await page.waitFor(() => !document.querySelectorAll('#checkout-loader').length);

    await page.type('#customer-email', user.email, opts);
    await page.type('input[name="firstname"]', user.first, opts);
    await page.type('input[name="lastname"]', user.last, opts);
    await page.type('input[name="street[0]"]', user.street, opts);
    await page.type('input[name="postcode"]', user.zip, opts);
    await page.type('input[name="city"]', user.city, opts);
    await page.select('select[name="region_id"]', user.state_co_value);
    await page.type('input[name="telephone"]', user.phone);
    await page.click('button[data-bind*="setShippingInformation"]');
    await page.waitFor(
        () => document.querySelectorAll('button[data-bind*="continueToReview"]').length > 0,
        { timeout: 60000 }
    );
    await page.waitFor(() => !document.querySelectorAll('#checkout-loader').length);
}

module.exports = {
    checkoutAsGuest,
    goToCart
};
