FROM node:12.18.3-buster-slim

# Copy app
COPY . /app

# Build app
RUN cd /app; \
    npm install;

# Run app
CMD node /app/index.js
